//
//  DismissButton.swift
//  Designable
//
//  Created by Shantini Vyas on 8/17/18.
//  Copyright © 2018 Shantini Vyas. All rights reserved.
//

import UIKit
import Observable

@IBDesignable
class ImageButton: UIView {
    
    var imageView: UIImageView = UIImageView()
    
    var button: UIButton = UIButton()
    
    @IBInspectable var image: UIImage? {
        didSet {
            
            imageView.removeFromSuperview()
            
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            imageView.backgroundColor = .clear
            
            backgroundColor = .clear
            
            let height = frame.height
            let width = frame.width
            let imageHeight = height/2
            imageView.frame = CGRect(x: width/2 - imageHeight/2, y: height/2 - imageHeight/2, width: imageHeight, height: imageHeight)
            addSubview(imageView)
            sendSubview(toBack: imageView)
            
        }
    }
    
    @IBInspectable var tint: UIColor = .white {
        didSet{
            imageView.tintColor = tint
        }
    }
    
    typealias ButtonPress = (view: ImageButton, button: UIButton)
    var buttonPress: Event<ButtonPress> = Event<ButtonPress>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        button.removeFromSuperview()
        button.frame = bounds
        button.backgroundColor = .clear
        addSubview(button)
        bringSubview(toFront: button)
        
        button.addTarget(self, action: #selector(didPressButton), for: .touchUpInside)

    }
    
    func didPressButton () {
        buttonPress.notify(ButtonPress(self, button))
        print("button press")
    }

}
