//
//  ToggleButton.swift
//  Designable
//
//  Created by Shantini Vyas on 8/16/18.
//  Copyright © 2018 Shantini Vyas. All rights reserved.
//

import UIKit
import Observable

@IBDesignable
class ToggleButton: UISegmentedControl, AnyToggleButton {

    var segmentChange: Event<SegmentChanged>    = Event<SegmentChanged>()
    typealias SegmentChanged = (control: UISegmentedControl, segment: Int)
    
    //******** add @IBInspectable to these properties ********
    @IBInspectable var leftColor: UIColor = .blue {
        didSet {
            if selectedSegmentIndex == 0 {
                tintColor = leftColor
            }
        }
    }
    @IBInspectable var rightColor: UIColor = .red {
        didSet {
            if selectedSegmentIndex == 1 {
                tintColor = rightColor
            }
        }
    }
    @IBInspectable var backColor: UIColor  = .black {
        didSet {
            backgroundColor = backColor
        }
    }
    @IBInspectable var selectedTextColor: UIColor = .white {
        didSet {
            var colorAttr: [String : Any] = [:]
            colorAttr[NSForegroundColorAttributeName] = selectedTextColor
            setTitleTextAttributes(colorAttr, for: .selected)
        }
    }
    @IBInspectable var leftText: String = "" {
        didSet {
            setTitle(leftText, forSegmentAt: 0)
        }
    }
    @IBInspectable var rightText: String = "" {
        didSet {
            setTitle(rightText, forSegmentAt: 1)
        }
    }
    @IBInspectable var fontSize: Int = 12 {
        didSet {
            var sizeAttr: [String : Any] = [:]
            sizeAttr[NSFontAttributeName] = UIFont(name: "AvenirNext", size: CGFloat(fontSize))
            setTitleTextAttributes(sizeAttr, for: .normal)
        }
    }
    
   func segmentDidChange() {
        didSelectSegment(selectedSegmentIndex)
        if selectedSegmentIndex == 0 {
            tintColor = leftColor
        }
        else if selectedSegmentIndex == 1 {
            tintColor = rightColor
        }
    }
    
    func didSelectSegment(_ segment: Int) {
        segmentChange.notify(SegmentChanged(self, segment))
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addTarget(self, action: #selector(segmentDidChange), for: .valueChanged)
    }

}
