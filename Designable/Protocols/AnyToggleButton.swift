//
//  AnyToggleButton.swift
//  OwlApp
//
//  Created by Shantini Vyas on 8/13/18.
//  Copyright © 2018 725-1 Corporation. All rights reserved.
//

import Foundation
import UIKit
import Observable
//*** Remember to add the @IBDesignable decorator to your class!

protocol AnyToggleButton {
    
    var segmentChange: Event<SegmentChanged>    { get set }
    associatedtype SegmentChanged
    
    //******** add @IBInspectable to these properties ********
    var leftColor: UIColor                      { get set }
    var rightColor: UIColor                     { get set }
    var backColor: UIColor                      { get set }
    var selectedTextColor: UIColor              { get set }
    var leftText: String                        { get set }
    var rightText: String                       { get set }
    var fontSize: Int                           { get set }
    
    func didSelectSegment(_ segment: Int)
}
